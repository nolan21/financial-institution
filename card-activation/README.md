# README

## Custom Code Test Tools

Oracle Autonomous Mobile Cloud Enterprise, Version 18.1.1.0

2018-01-10

Oracle Autonomous Mobile Cloud Enterprise (AMCe) provides a set of custom code test tools to help you:

- Test and debug your custom API implementations on your local machine.
- Package and upload those API implementations to AMCe from the command line.

## What are the tools?

The set of tools consists of:

- An npm module (omce-tools) that includes the command-line tools for starting a local custom code container, running tests, and deploying an API implementation to AMCe.
- An API (OracleMobileAPI) to proxy AMCe API calls from the node container running locally on your machine to the container running within AMCe. You need to upload this API to AMCe and associate it with the backend that contains the API you want to work with.

## Prerequisites

- A local installation of Node.js (version 6.10 or compatible)
- The Developer role in an instance of AMCe

## Setting up the tools

### Installing `omce-tools`
1. On your machine, open a terminal window, and change directories to the `omce-tools` directory.
2. Run `npm install -g`.
3. To ensure the tools were installed correctly, run `omce-test --version`. It should return `18.1.1`.

### Adding OracleMobileAPI to AMCe

1. From the Development tab in AMCe, click **APIs**, then click **New API**, and select API.
2. Click **Upload a RAML document**, navigate to the OracleMobileAPI.raml file on your system, and click **Open**.
3. Type a short description for the API and click **Create**.
4. Click the **Security** tab and turn Login Required to the OFF position.
5. Click **Save**.
6. Click **Implementations**, upload `OracleMobileAPIImpl.zip` from your system, and click **Save**.

## Testing an API Implementation

#### Setting up Your API for Testing

1. If you haven't already done so, create your API in AMCe and add endpoints and sample request/response data.
2. In AMCe, associate the API with a backend. (Click Backends, select the backend you want to use and click Open, click the backend's APIs tab, and click Select APIs.)
3. Associate OracleMobileAPI with the same backend.
4. Download the JavaScript scaffold for the API that you want to test. (Open the API, click its Implementation tab, and click JavaScript Scaffold.)
5. Unzip the scaffold and check its contents. The directory should contain the following files:
    - `package.json` - the module manifest
    - `<api name>.js` - your starter implementation
    - `<api name>.raml` - the API definition in RAML format
    - `swagger.json` - the API definition in Swagger format
    - `toolsConfig.json` - contains metadata needed by the tools, such as backend environment and authorization info, the API, and AMCe endpoint and test definitions
    - `samples.txt`
    - `README.md` - this file

### Setting Up the API Implementation for Testing

1. In a terminal window, change directories to the directory containing your scaffold.
2. Run `npm install`
3. Update the API implementation's `toolsConfig.json` file to include your AMCe instance, backend and authorization info.
    - `baseUrl` is the base URL of your AMCe instance. This value is required and may be found on the **Development -> Backend -> Settings** page in AMCe.
    - `tokenEndpoint` is your tenant's IDCS OAuth token endpoint. This value is required and may be found on the **Development -> Instance Details** page in AMCe.
    - `backend` properties are required to authorize API calls made by your API implementation to your AMCe instance via omce-ccc. You can get the backend info from the backend's Settings page. (From the Development tab in AMCe, click **Backends**, select the backend that uses the API, click **Open**, and click **Settings**.). The following properties are required:
      - `backend.backendId`
      - `backend.authorization.anonymousKey`
      - `backend.authorization.clientId`(optional, required to use oauth security when submitting tests using `omce-test`)
      - `backend.authorization.clientSecret`(optional, required to use oauth security when submitting tests using omce-test)
    - `tools` properties are required by commands such as omce-deploy, that use the AMCe public tooling APIs. The IDCS clientId and clientSecret can be found on the **Mobile Apps -> Instance Details** page in AMCe. The following properties are required:
      - `tools.authorization.clientId`
      - `tools.authorization.clientSecret`

By default, the tools assume `toolsConfig.json` is co-located with or in the same directory as your API implementation. If you choose to move `toolsConfig.json` to a different directory, you must specify the location of the API implementation in `toolsConfig.json` using moduleLocation.

Other possible `toolsConfig.json` properties, including the format of tests, are documented in the `resource/configMetadata.json` file.

### Starting the Local Container

This command starts a node container running your API implementation (as identified by `toolsConfig.json`). The container can be started with or without the `--debug` option. If you start the container with the `--debug` option, you will get a URL that you can use to open a debug session in Google Chrome.

`omce-ccc <path to toolsConfig.json> [--debug] [--verbose]`

### Making API Calls to the Container

Once the container is running, you may send requests to the container using `omce-test` (see **Running Tests on Your Implementation** below), cURL, Postman, or other REST clients.

By default, the container runs on port 4000. (You can change this in the `resource/configMetadata.json` file.)

With cURL, a simple command might look something like:

```$ curl -X GET http://localhost:4000/mobile/custom/<api-name>/<api-version>/<resource-path>```

## Offline Container Options

`--debug`
Provides a URL you can use to open a debug session in Google Chrome.
`--verbose`
If you have errors or warnings, will show examples of the missing property in addition to a description of the property.
`--version`
Get the version of any tool.

### Notes on the Offline Container

There are some differences between the custom code container in AMCe and the offline container. The offline container:
  - Has a less granular set of methods for logging. Calls to `console.finest`, `console.finer`, `console.fine`, `console.config`, and `console.info` methods are treated as calls to `console.log`. Calls to `console.warning` are treated as calls to `console.warn`. Calls to `console.severe` are treated as calls to `console.error`.
  - Does not do any automatic logging.
  - Does not catch unhandled errors. Instead, unhandled errors stop the container.


## Running Tests on Your Implementation

Once the container is running, you can submit requests to the container using the testing tool. The tests are defined in the implementation's `toolsConfig.json` and you can add or remove tests as needed. The initial set of tests is generated from sample data you entered while defining your resources & methods in AMCe. For example:

```
"postPets":{
    "method":"POST",
    "resource":"/pets",
    "payload":{
        "name":"Oreo",
        "species":"Cat",
        "breed":"Domestic Short Hair",
        "age":"14",
        "vetId":"blueRidge"
    }
},
"getPets":{
    "method":"GET",
    "resource":"/pets"
},
"getPetsId":{
    "method":"GET",
    "resource":"/pets/:id",
    "uriParameters":{
        "id":"<PARAMETER_VALUE>"
    }
}
```

To execute a test, use one of the following commands:

```
omce-test <path to toolsConfig.json> <test name> [--security anonymous] [--verbose]
    
omce-test <path to toolsConfig.json> <test name> --security basic [--username ] [--password <mobile user password>] [--verbose]

omce-test <path to toolsConfig.json> <test name> --security oauth [--username <mobile user>] [--password <mobile user password>] [--verbose]
```

If you don't specify the username or password on the command-line when using the `basic` or `oauth` options, you will be prompted to enter the values when the command runs.

For example, the following command returns the status code and payload contained in the response:

`omce-test ../apis/1.0/toolsConfig.json postPets --security oauth`

You can continue executing tests as long as the container remains running.

## Packaging and Uploading Your Implementation

Once you have completed your implementation or reached a stage where you are ready to test it in AMCe, you can use the deployment tool (`omce-deploy`) to package the implementation in a zip file and upload it to AMCe.

`omce-deploy` can be issued in these forms:

```
omce-deploy <toolsConfig.json> -u <AMCe team member> -p <pass> [--verbose]

omce-deploy <toolsConfig.json> --username <AMCe team member> --password <pass> [--verbose]

omce-deploy <toolsConfig.json> -u <AMCe team member> [--verbose]

omce-deploy <toolsConfig.json> [--verbose]
```

For example:

`omce-deploy ../testaccess/apis/pets3/1.0/toolsConfig.json`
If you don't specify the username and password on the command-line, you will be prompted to enter the values when the command runs.

**Note**: The username and password required for omce-deploy are for an AMCe team member. For API tests that you run using omce-test, the user name and password correspond to a mobile user.

## Change Log

18.1.1 (AMCe)
+ Updated command names from mcs-* to omce-* to allow tools for internal compute and external compute to be installed concurrently

17.4.5 (OMCe)
- Updated OAuth logic for omce-deploy to obtain an OAuth token direct from IDCS instead of via a backend.
- Modified the structure of toolsConfig.json to support both backend authorization and tools authorization properties. You may continue to use existing toolsConfig.json files for testing your APIs but omce-deploy requires additional parameters not included in the original format.

17.2.5.1 (Mobile Cloud Service)
- Fixed an issue that affected OAuth authorization using omce-test.

17.2.5 (Mobile Cloud Service)
- Fixed an issue that affected remote REST APIs calls.

17.2.1 (Mobile Cloud Service)
- Initial release.