from flask import Flask, request, jsonify, render_template, send_file
from twilio.rest import Client
from os import environ
from string import digits
import json, random, base64, requests
import cc_ocr as ocr

app = Flask(__name__)
app.debug = True

account_sid = "ACe944a6da0dcf73ffc91faae45c95a8d5"
auth_token = "de4b7dbad8a347a2b863e9f8f945b698"
twilio_number = "+18325723251"
client = Client(account_sid, auth_token)

@app.route("/hello")
def hello():
  return "Hello World!"
  

@app.route("/read_card", methods=['POST'])
def get_card_number():
  img_url = request.form['imageUrl']
  raw_data = requests.get(img_url).content
  img64 = base64.b64encode(raw_data)
  card_type, card_number = ocr.get_card_number(img64)
  return jsonify({
    "success": True,
    "card": {
      "type": card_type,
      "number": card_number
    }
  })


@app.route("/check_card", methods=['GET'])
def check_card():
  mobile = request.args.get("mobile")
  card_number = request.args.get("cardNumber")
  cvc = request.args.get("cvc")
  with open('./data/users.json', 'r') as f:    
    users = json.load(f)
  if users[mobile]['card_number'][-4:] == card_number and users[mobile]['cvc'] == cvc:     
    users[mobile]['activated'] = True
    with open('./data/users.json', 'w') as f:  
      json.dump(users, f)
    return jsonify({
      "success": True,
      "message": users[mobile]
    })
  return jsonify({
    "success": False,
    "user": users[mobile]
  })


@app.route("/download.pkpass", methods=['GET'])
def download_pkpass():
  cc = request.args.get("cc")
  try:
    return send_file("./data/pkpass_files/"+cc+".pkpass", mimetype="application/vnd.apple.pkpass", attachment_filename="pscu.pkpass", as_attachment=True)
  except Exception as e:
    return str(e)

if __name__ == "__main__":
  port = int(environ.get('PORT', 8888))
  app.run(host="0.0.0.0", port=port)