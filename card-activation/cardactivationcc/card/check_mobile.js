"use strict";

var log4js = require('log4js');
var logger = log4js.getLogger();
var request = require('request');

const accountSid = "ACe944a6da0dcf73ffc91faae45c95a8d5";
const authToken = "de4b7dbad8a347a2b863e9f8f945b698";
const twilioNumber = "+18325723251";
const client = require('twilio')(accountSid, authToken);

module.exports = {

  metadata: function metadata() {
    return {
      "name": "CheckMobile",
      "properties": {
        "phoneNumber": { "type": "string", "required": true }
      },
      "supportedActions": [ "valid", "invalid" ]
    };
  },

  invoke: (conversation, done) => {
    var amce = conversation.oracleMobile;
    var phoneNumber = conversation.properties().phoneNumber;
    
    var logMsg = '\n\nCheckMobile - ' + phoneNumber + '\n______________________________________________________________\n'
    logMsg +=  '______________________________________________________________\n';
    console.log(logMsg);
    
    amce.storage.getById("Customers", phoneNumber)
      .then(result => {
        console.log("Customer found: " + result.result);
        var customer = JSON.parse(result.result);
        customer.authCode = (""+Math.floor(Math.random()*9000000)+1000000).substring(2,8);
        var authMsg = "Your 6 digit verification code is: " + customer.authCode;
        var name = customer.name.split(" ");
        var first = name[0];
        var last = name[1];
        var addresses = customer.address;
        for (var i=0; i<customer.other_addresses.length; i++) {
          addresses += ", " + customer.other_addresses[i];
        }
        conversation.variable("addresses", addresses);
        conversation.variable("profile.firstName", first);
        conversation.variable("profile.lastName", last);
        conversation.variable("validAuthCode", customer.authCode);
        conversation.variable("validAddress", customer.address);
        
        client.messages.create({
          body: authMsg = "Your 6 digit verification code is: " + customer.authCode,
          from: twilioNumber,
          to: phoneNumber
        })
        .then(message => {
          console.log("Twilio Message SID: " + message.sid);
          console.log("Storing updated customer object with active authCode: " + customer);
          amce.storage.storeById('Customers', phoneNumber, JSON.stringify(customer))
          .then(result => {
            console.log(result.result);
            conversation.transition("valid");
            done();
          })
        });
      }, error => {
        console.log("Customer not found: " + error.error);
        conversation.transition("invalid");
        done();
      });

    /* var options = { 
      method: 'GET',
      url: 'https://creditunionapi-gse00014110.uscom-east-1.oraclecloud.com/check_mobile?mobile=' + phoneNumber,
    };

    request(options, function (err, res, body) {
      if (err) throw new Error(err);
      var logMsg = '\n\nCheckMobile\n______________________________________________________________\n'
      logMsg +=  '______________________________________________________________\n'+body+'\n\n'
      console.log(logMsg);
      var data = JSON.parse(body);
      if (data.success) {
        var name = data.user["name"].split(" ");
        var first = name[0];
        var last = name[1];
        var addresses = data.addresses[0];
        for (var i=1; i<data.addresses.length; i++) {
          addresses += ", " + data.addresses[i];
        }
        conversation.variable("addresses", addresses);
        conversation.variable("profile.firstName", first);
        conversation.variable("profile.lastName", last);
        conversation.variable("validAuthCode", data.user["authCode"]);
        conversation.variable("validAddress", data.user["address"]);
        conversation.transition("valid");
      } else {
        conversation.transition("invalid");
      }
      done(); 
    });*/
  }
};
