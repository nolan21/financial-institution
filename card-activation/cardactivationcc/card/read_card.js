"use strict";

var log4js = require('log4js');
var logger = log4js.getLogger();
var request = require('request');
var fs = require('fs');
var Tesseract = require('tesseract.js');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var imageFile = 'imageFile.png'

var getBlob = (url) => {
  var blob = null;
  var xhr = new XMLHttpRequest(); 
  xhr.open("GET", url); 
  xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
  xhr.onload = () => {
    blob = xhr.response;//xhr.response is now a blob object
    console.log(blob);
    return blob;
  }
}

module.exports = {

  metadata: function metadata() {
    return {
      "name": "ReadCard",
      "properties": {
        "cardImage": { "type": "string", "required": true }
      },
      "supportedActions": []
    };
  },

  invoke: (conversation, done) => {
    //var writeFile = fs.createWriteStream(imageFile);
    var imageUrl = JSON.parse(conversation.properties().cardImage)["url"];
    console.log(imageUrl);
    var options = { 
      method: 'POST',
      url: 'https://cardactivationapi-gse00015257.uscom-east-1.oraclecloud.com/read_card',
      form: { imageUrl: imageUrl }
    };

    request(options, function (err, res, body) {
      if (err) throw new Error(err);
      var data = JSON.parse(body);
      var logMsg = '\n\nReadCard\n______________________________________________________________\n'
      logMsg +=  '______________________________________________________________\n'+data+'\n\n'
      console.log(logMsg);
      if (data.success) {
        conversation.variable("cardType", data.card.type);
        conversation.variable("cardNumber", data.card.number);
        conversation.keepTurn(true);
        conversation.transition(true); 
      } else {
        conversation.reply({"text": "There seems to have been a problem."})
        conversation.transition();
      }
      done();
    });
  }
};