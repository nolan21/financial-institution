'use strict';

module.exports = {
  components: {
    'CheckMobile': require('./card/check_mobile'),
    'ReadCard': require('./card/read_card'),
    'CheckCard': require('./card/check_card'),
    'AddToWallet': require('./card/add_to_wallet')
  }
};