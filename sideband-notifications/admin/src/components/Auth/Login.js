import React, { Component } from 'react';
import { Button, Form, Grid, Header, Image, Message, Segment, Container } from 'semantic-ui-react';
import logo from '../../images/login-logo2.png';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as amceActions from '../../redux/actions/amceActions';
import PropTypes from 'prop-types';

import { Link, Redirect } from 'react-router-dom';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      username: null,
      password: null,
      redirect: false,
      loginFailed: false,
    };
  }

  updateUsername = (e) => { this.setState({username: e.target.value}); }
  updatePassword = (e) => { this.setState({password: e.target.value}); }

  login = () => { 
    console.log("Logging in...");
    let { username, password } = this.state;
    this.props.amceActions.login(username, password)
      .then(result => {
        console.log(result);
        this.setState({ redirect: true });
      }, error => {
        console.log(error);
        this.setState({ loginFailed: true });
      });
  }

  render() {
    const { redirect, loginFailed } = this.state;
    if (redirect) return <Redirect to='/'/>;
    return (
      <div className='proper-height'>
        <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' textAlign='center' style={{ color: "#006A8C" }}>
              <Image size='large' src={logo} style={{ height: "auto", width: "30%" }}/> Login to your account
            </Header>
            <Form size='large'>
              <Segment stacked>
                <Form.Input 
                  fluid 
                  icon='user' 
                  iconPosition='left' 
                  placeholder='username'
                  value={this.state.username}
                  onChange={this.updateUsername} 
                />
                <Form.Input
                  fluid
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password'
                  type='password'
                  value={this.state.password}
                  onChange={this.updatePassword}
                />
                <Button color='teal' fluid size='large' 
                  onClick={this.login}>
                  Log In
                </Button>
              </Segment>
            </Form>
            <Message>
              New to us? <Link to='/register'>Sign Up</Link>
            </Message>
          </Grid.Column>
        </Grid>        
      </div>
    );
  }
}

Login.propTypes = {
  amceActions: PropTypes.object,
  backend: PropTypes.object,
  loggedIn: PropTypes.bool,
  accessToken: PropTypes.string,
  username: PropTypes.string
};

function mapStateToProps(state) {
  return {
    backend: state.amce.backend,
    loggedIn: state.amce.loggedIn,
    accessToken: state.amce.accessToken,
    username: state.amce.username
  };
}

function mapDispatchToProps(dispatch) {
  return {
    amceActions: bindActionCreators(amceActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);