import React, { Component } from 'react';

import mcs from 'mcs';
import mcsConfig from '../mcs_config';
mcs.init(mcsConfig);

// Please create collection in storage section of MCS UI and put here the name of the collection.
const collectionId = 'YOUR_STORAGE_COLLECTION_NAME';
// Please upload some picture to collection and put here the id of this picture.
const pictureObjectId = 'YOUR_PICTURE_OBJECT_ID';
// Please put here the file name that it will be saved in colelection with.
const fileName = 'YOUR_FILE_NAME';
// Please change this content type if your file is different than just plain text
const contentType = 'text/plain'; 

class AMCe extends Component {
  constructor(props) {
    console.log("The config has been set and now it has the backend defined in the config as the point of entry for the rest of the functions you need to call.");
    super(props);
    this.state = { 
      backend: mcs.mobileBackend
    };
  }
  /**
   * Login MCS with username and password
   * @param {string} authorizationType
   * @param {string} username
   * @param {string} password
   * @return {Promise<NetworkResponse>}
   */
  login(authorizationType, username, password) {
    this.backend.setAuthenticationType(authorizationType);
    return this.backend
      .authorization
      .authenticate(username, password)
      .then(response => {
        console.log('MCSService', 'application logged in', response);
        return response;
      });
  }
 
  /**
   * Download image from server
   * @return {Promise<StorageObject|NetworkResponse>}
   */
  download(collectionId, pictureObjectId){
    return this._getCollection(collectionId)
      .then(collection => this._readPictureObject(collection, pictureObjectId));
  }
 
  /**
   * Upload file to collection
   * @return {Promise<StorageObject|NetworkResponse>}
   */
  upload(collectionId, fileName, payload) {
    return this._getCollection(collectionId)
      .then(collection => {
        var object = new mcs.StorageObject(collection);
        object.setDisplayName(fileName);
        object.loadPayload(payload, contentType);
        return this._postObject(collection, object);
      });
  }
 
  /**
   * Logout from MCS
   */
  logout() {
    if (this.backend.authorization) {
      this.backend
        .authorization
        .logout();
    }
  }

  /**
   * Get collection details from server
   * @return {Promise<StorageCollection|NetworkResponse>}
   */
  _getCollection(collectionId){
    return this.backend
      .storage
      .getCollection(collectionId)
      .then(collection => {
        console.log('Loaded collection', collection);
        return collection;
      })
      .catch(error => {
        console.log('Failed to download storage collection', error);
        return Promise.reject(error);
      });
  }
 
  /**
   * Read object from collection
   * @param {StorageCollection} collection
   * @param {string} id - object id
   * @return {Promise<StorageObject|NetworkResponse>}
   */
  _getObject(collection, id) {
    return collection
      .getObject(id, 'blob')
      .catch(error => {
        console.log('Failed to read storage object', error);
        return Promise.reject(error);
      });
  }
 
  /**
   * Send object to server
   * @param {StorageCollection} collection
   * @param {StorageObject} obj
   * @return {Promise<StorageObject|NetworkResponse>}
   */
  _postObject(collection, object){
    return collection
      .postObject(object)
      .then(response => {
        console.log('Posted storage object', response.storageObject);
        return response.storageObject;
      })
      .catch(error => {
        console.log('Error posting the storage object', error);
        return Promise.reject(error);
      });
  }
  render() {
    return null;
  }
}

export default AMCe;