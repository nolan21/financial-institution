import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loanListActions from '../redux/actions/loanListActions';
import * as amceActions from '../redux/actions/amceActions';
import { 
  Container, Divider, Grid, Header, Image, Table,
  Dropdown,  Segment, List, Button
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
/* import mediaParagraph from '../images/wireframes/media-paragraph.png';
import paragraph from '../images/wireframes/paragraph.png';
import image from '../images/wireframes/image.png'; */
import config from '../config';
import axios from 'axios';

import LoadingState from './LoadingState';

/* import { 
  Container, Row, Col, Button, Alert, 
  Card, CardBody, CardTitle, CardLink
} from 'reactstrap'; */
import Loan from './Loan';

class LoanList extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      success: false
    };
  }

  componentDidMount = () => { if (this.props.loggedIn) this.props.loanListActions.fetchLoans(); }
  
  deleteProfile = (index) => {
    var loans = this.props.loans;
    config.HEADERS.data = {
      cudlId: loans[index].cudlId,
      token: this.props.token
    }
    axios.delete(config.BASE_URL+'/loans/'+loans[index].mcsId, config.HEADERS, {
      cudlId: loans[index].cudlId,
      token: this.props.token
    })
      .then(res => {
        console.log(res.data);
      });
    loans.splice(index, 1);
    this.props.loanListActions.updateLoans(loans);
  }
  
  updateStatus = (index, newStatus) => {
    var loan = this.props.loans[index];
    console.log(newStatus);
    loan.status = newStatus;
    loan.lastNotification = new Date().toLocaleString();
    var msg = {
      from: "Credit Union Loan Processor", 
      to: loan.contact_name, 
      subject: "Loan Status Updated",
      message: (loan.status === "Current") ?
      "The status of this loan has been changed to \"Current\" again!" : 
      "The status of this loan has been changed to \"Completed\"!",
      time: loan.lastNotification,
      unread: true
    }
    loan.messages.push(msg);
    loan.notification = {
      message: msg,
      token: this.props.token
    }
    axios.put(config.BASE_URL+'/loans/'+loan.mcsId, loan, config.HEADERS)
      .then(res => {
        delete loan.notification;
        this.props.loanListActions.updateLoan(index, loan)
      });
  }
  
  getLoans = () => {
    var loans = [];
    for (var i = 0; i < this.props.loans.length; i++) {
      loans.push(
        <Loan loan={this.props.loans[i]} 
              index={i} 
              deleteProfile={this.deleteProfile.bind(this)}
              updateStatus={this.updateStatus}
              unreadMsg={this.props.unreadMsg}>
        </Loan>
      );
    }
    return (
      <Table.Body>{loans}</Table.Body>
    );
  }
  
  handleSort = (param) => () => {
    if (this.props.column !== param) {
      this.props.loanListActions.sortLoans(param)
      return
    }
    this.props.loanListActions.sortLoans()
  }

  render() {
    const { loggedIn, column, direction, unreadMsg, loans } = this.props;
    if (!loggedIn) return <Redirect to='/login' />
    if (!loans[0]) return <LoadingState />
    return (
      <div>
        <Container fluid style={{ marginTop: '7em' }}>
          <Grid>
            <Grid.Row>
              <Grid.Column width={1} />
              <Grid.Column width={14}>
                <Segment.Group style={{overflowX: "scroll"}}>
                  <Segment style={{paddingLeft: "28px", paddingTop: "28px", paddingBottom: "80px"}}>
                    <Header size="large" className="LoanListTitle">Dealer Notifier</Header>
                    <Link to='/create_profile'>
                      <Button size="big" className="float-right" style={{fontSize: "20px", color: "black"}}>
                        Create Notification Profile
                      </Button>
                    </Link>
                  </Segment>
                  <Segment.Group>
                    <Table sortable striped selectable unstackable>
                      <Table.Header>
                        <Table.Row className="title">
                          <Table.HeaderCell sorted={column === 'cudlId' ? direction : null} onClick={this.handleSort('cudlId')}>Loan ID</Table.HeaderCell>
                          <Table.HeaderCell sorted={column === 'loanee' ? direction : null} onClick={this.handleSort('loanee')}>Loanee</Table.HeaderCell>
                          <Table.HeaderCell sorted={column === 'value' ? direction : null} onClick={this.handleSort('value')}>Loan Value</Table.HeaderCell>
                          <Table.HeaderCell sorted={column === 'dealership' ? direction : null} onClick={this.handleSort('dealership')}>Dealership</Table.HeaderCell>
                          <Table.HeaderCell sorted={column === 'contact_name' ? direction : null} onClick={this.handleSort('contact_name')}>Dealer Contact</Table.HeaderCell>
                          <Table.HeaderCell sorted={column === 'status' ? direction : null} onClick={this.handleSort('status')}>Status</Table.HeaderCell>
                          <Table.HeaderCell sorted={column === 'lastNotification' ? direction : null} onClick={this.handleSort('lastNotification')}>Last Notification</Table.HeaderCell>
                          <Table.HeaderCell>Actions</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      {this.getLoans()}
                    </Table>
                  </Segment.Group>
                </Segment.Group>
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          </Grid>
        </Container>
      </div>
/*       
      <Container fluid="true">
        {
          unreadMsg.unread ? (
          <Alert color="warning" style={{padding: "25px"}}>
            <h3>
              You have {unreadMsg.profiles.length} unread message{unreadMsg.profiles.length > 1 ? "s" : ""}!
            </h3>
          </Alert>) : null
        }
        <br/><br/>
        <Row>
          <Col sm="12" md={{ size: 10, offset: 1 }} style={{paddingLeft: "0px", paddingRight: "0px"}}>
            <Card body style={{paddingLeft: "0px", paddingRight: "0px"}}>
              {
                this.props.success ? (
                <Alert color="success">
                  <h2 className="alert-heading">Success!</h2>
                  <p>
                    The loan notification profile has been successfully saved!
                  </p>
                </Alert>) : null
              }
              <CardTitle>
                <Container>
                <Row>
                  <Col sm="12">
                    <h1 style={{fontSize: "35px"}}>Dealer Notifier</h1>
                  </Col>
                </Row>
                </Container>
              </CardTitle>
              <CardLink>
                <Container>
                <Col>
                  <Link to='/create_profile'>
                    <Button outline color="primary" size="lg" className="float-right" style={{fontSize: "15px"}}>
                      Create Notification Profile
                    </Button>
                  </Link>
                </Col>
                </Container>
              </CardLink>
              <CardBody>
                <Container style={{overflow: "visible"}}>
                  <Table sortable striped selectable stackable collapsing style={{fontSize: "15px", width: "inherit", overflow: "visible"}}>
                    <Table.Header>
                      <Table.Row className="title">
                        <Table.HeaderCell sorted={column === 'cudlId' ? direction : null} onClick={this.handleSort('cudlId')}>Loan ID</Table.HeaderCell>
                        <Table.HeaderCell sorted={column === 'loanee' ? direction : null} onClick={this.handleSort('loanee')}>Loanee</Table.HeaderCell>
                        <Table.HeaderCell sorted={column === 'value' ? direction : null} onClick={this.handleSort('value')}>Loan Value</Table.HeaderCell>
                        <Table.HeaderCell sorted={column === 'dealership' ? direction : null} onClick={this.handleSort('dealership')}>Dealership</Table.HeaderCell>
                        <Table.HeaderCell sorted={column === 'contact_name' ? direction : null} onClick={this.handleSort('contact_name')}>Dealer Contact</Table.HeaderCell>
                        <Table.HeaderCell sorted={column === 'status' ? direction : null} onClick={this.handleSort('status')}>Status</Table.HeaderCell>
                        <Table.HeaderCell sorted={column === 'lastNotification' ? direction : null} onClick={this.handleSort('lastNotification')}>Last Notification</Table.HeaderCell>
                        <Table.HeaderCell>Actions</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    {this.getLoans()}
                  </Table>
                </Container>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <br/><br/>
      </Container> */
    );
  }
}

LoanList.propTypes = {
  loanListActions: PropTypes.object,
  amceActions: PropTypes.object,
  loans: PropTypes.array,
  column: PropTypes.string,
  direction: PropTypes.string,
  unreadMsg: PropTypes.object,
  unread: PropTypes.bool,
  profiles: PropTypes.array,
  success: PropTypes.bool,
  token: PropTypes.string,
  backend: PropTypes.object,
  loggedIn: PropTypes.bool,
  accessToken: PropTypes.string,
  username: PropTypes.string  
};

function mapStateToProps(state) {
  return {
    loans: state.loanList.loans,
    column: state.loanList.column,
    direction: state.loanList.direction,
    unread: state.loanList.unreadMsg.unread,
    profiles: state.loanList.unreadMsg.profiles,
    unreadMsg: state.loanList.unreadMsg,
    success: state.loanList.success,
    token: state.loanList.token,

    backend: state.amce.backend,
    loggedIn: state.amce.loggedIn,
    accessToken: state.amce.accessToken,
    username: state.amce.username
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loanListActions: bindActionCreators(loanListActions, dispatch),
    amceActions: bindActionCreators(amceActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoanList);

/*
  componentDidMount = () => {
    if (this.props.loggedIn) this.props.loanListActions.fetchLoans();
    
    //this.props.loanListActions.fetchLoans();
    //var token = this.props.backend.authorization.getAccessToken();
    //this.setState({token: token});
    this.interval = setInterval(() => {
      if (typeof this.props.unreadMsg === 'undefined') { return }
      var unread = false;
      var profiles = [];
      var unreadMsg = this.props.unreadMsg;
      for (var i = 0; i < this.props.loans.length; i++) {
        var msgs = this.props.loans[i].messages;
        for (var j = 0; j < msgs.length; j++) {
          if (msgs[j].to === "Credit Union Loan Processor" && msgs[j].unread === true) {
            profiles.push(i);
            unread = true;
          }
        }
      }
      unreadMsg.unread = unread;
      unreadMsg.profiles = profiles;
      this.props.loanListActions.updateUnread(unreadMsg);
    }, 1500);
    if (this.props.location.state && this.props.location.state.success) {
      this.setState({ success: !this.props.success });
      setTimeout(function(){
        this.setState({ success: !this.props.success });
      }.bind(this), 2500);
    } 
  }; 
  
  componentWillUnmount = () => {
    clearInterval(this.interval);
  }; 
*/