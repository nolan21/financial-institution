import { combineReducers } from 'redux';
import loanListReducer from './loanListReducer';
import amceReducer from './amceReducer';

const rootReducer = combineReducers({
  amce: amceReducer,
  loanList: loanListReducer
});

export default rootReducer;