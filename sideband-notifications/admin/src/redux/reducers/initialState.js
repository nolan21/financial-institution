export default {
  loanList: {
    loans: [],
    column: null,
    direction: null,
    unreadMsg: {},
    success: null,
    token: null
  },
  amce: {
    loggedIn: false,
    accessToken: null,
    username: null,
    firstName: null,
    lastName: null,
    email: null,
    id: null
  }
};