import initialState from './initialState';
import {
  SET_AUTH
} from '../actions/actionTypes';

export default function amceReducer(state = initialState.amce, action) {
  let newState;
  switch (action.type) {
    case SET_AUTH:
      return {
        ...state,
        loggedIn: action.loggedIn,
        accessToken: action.accessToken,
        username: action.username,
        firstName: action.firstName,
        lastName: action.lastName,
        email: action.email,
        id: action.id
      }

    default:
      return state;
  }
}