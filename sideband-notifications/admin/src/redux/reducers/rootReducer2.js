import { combineReducers } from 'redux';
import loanListReducer from './loanListReducer';
import amceReducer from './amceReducer';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/es/storage'; // default: localStorage if web, AsyncStorage if react-native

/* const rootPersistConfig = {
  key: 'root',
  storage: storage
};
const authPersistConfig = {
  key: 'auth',
  storage: storage
}

const combinedReducer = combineReducers({
  amce: persistReducer(authPersistConfig, amceReducer),
  loanList: loanListReducer
});
const rootReducer = persistReducer(rootPersistConfig, combinedReducer);
export default rootReducer; */

const rootPersistConfig = {
  key: 'root',
  storage: storage
};
const combinedReducer = combineReducers({
  amce: amceReducer,
  loanList: loanListReducer
});
const rootReducer = persistReducer(rootPersistConfig, combinedReducer);
export default rootReducer;