import * as types from './actionTypes';
import _ from 'lodash';
import axios from 'axios';
import config from '../../config';
import amce from '../../services/AMCe';

const endpoints = {
  storage: "/mobile/platform/storage/collections/",
  notifications: "/mobile/system/notifications/notifications",
  devices: "/mobile/platform/devices",
  users: "/mobile/platform/users",
  policies: "/mobile/platform/appconfig/client"
}

export const fetchLoans = () => {
  console.log("(ACTION) fetchLoans()")
  return (dispatch, getState) => {
    var auth = getState().amce.accessToken;
    return amce.storage.getEach('Loans', auth)
      .then(result => {
        console.log(result);
        var loans = result.map(loan => loan.data);
        loans = _.sortBy(loans, ['status', 'loanee'])
        dispatch({type: types.GET_LOANS, loans: loans})
      });
  };
}

export const getLoans = (loans) => {
  return {type: types.GET_LOANS, loans: loans};
}

export const fetchToken = () => {
  return dispatch => {
    return axios.get(config.BASE_URL+'/dealers', config.HEADERS)
      .then(res => {
        dispatch(getToken(res.data.token));
      })
  }
}

export const getToken = (token) => {
  return {type: types.GET_TOKEN, token: token};
}

export const updateLoan = (index, loan) => {
  return {type: types.UPDATE_LOAN, index: index, loan: loan};
}

export const updateLoans = (loans) => {
  return {type: types.UPDATE_LOANS, loans: loans}
}

export const updateUnread = (unreadMsg) => {
  return dispatch => {
    dispatch({type: types.UPDATE_UNREAD, unreadMsg: unreadMsg});
  }
}

export const sortLoans = (param=null) => {
  return {type: types.SORT_LOANS, param: param}
}

export const clearLoans = () => {
  return {type: types.UPDATE_LOANS, loans: []}
}