import * as types from './actionTypes';
import mcs from 'mcs';
import config from '../../config';
import amce from '../../services/AMCe';
import axios from 'axios';
import { Base64 } from 'js-base64';

export const fetchBackend = () => {
  console.log("Fetching backend...");
}

/* export const createUser = () => {
  return (dispatch, getState) => {
    const { loanList, amce } = getState();
    return axios.post()
  }
} */

export const setAuth = (auth=null, user=null) => {
  return { 
    type: types.SET_AUTH, 
    loggedIn: auth ? true : false, 
    accessToken: auth ? auth.access_token : null,
    username: user ? user.username : null,
    firstName: user ? user.name.givenName : null,
    lastName: user ? user.name.familyName : null,
    email: user ? user.emails[0].value : null,
    id: user ? user.id : null,
  }
}

export const login = (username, password) => {
  return (dispatch, getState) => {
    return amce.auth.login(username, password)
      .then(result => {
        let auth = result.data;
        console.log(auth);
        return amce.auth.getUser(username)
          .then(result => {
            console.log(result.data);
            var user = result.data.Resources[0];
            dispatch(setAuth(auth, user));
            return auth;
          })
      });
  }
}

export const register = (first, last, email, username, password) => {
  return (dispatch, getState) => {
    var groupId = config.auth.groupIds.LoanProcessors;
    return amce.auth.register(first, last, email, username, password, groupId)
      .then(result => {
        console.log(result.data);
        return result;
      });
  }
}

export const logout = () => {
  return dispatch => { dispatch(setAuth()) }
}

/*
  username = (username.indexOf("@") > -1) ? 
  encodeURIComponent(username).replace(/%20/g, '+') :
  encodeURIComponent(username).replace(/%5B/g, '[').replace(/%5D/g, ']');
  password = (password.indexOf("&") > -1) ?
  encodeURIComponent(password).replace(/%20/g, '+') :
  encodeURIComponent(password).replace(/%5B/g, '[').replace(/%5D/g, ']');
*/