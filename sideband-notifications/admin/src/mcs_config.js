import mcs from 'mcs';
 
const mcsConfig = {
  "logLevel": mcs.LOG_LEVEL.NONE,
  "oAuthTokenEndPoint": "https://idcs-7cc4cc9dc5e84b87a4700cbce62def5b.identity.oraclecloud.com/oauth2/v1/token",
  "disableAnalyticsLocation": false,
  "mobileBackend": {
    "name": "CreditUnion",
    "baseUrl": "https://8BC26D71FCD04E74B3019AFDE5264E59.mobile.ocp.oraclecloud.com:443",
    "authentication": {
      "type": mcs.AUTHENTICATION_TYPES.basic,
      "basic": {
        "mobileBackendId": "570b882b-876f-4a70-9399-c5fa5c79c507",
        "anonymousKey": "OEJDMjZENzFGQ0QwNEU3NEIzMDE5QUZERTUyNjRFNTlfTW9iaWxlQW5vbnltb3VzX0FQUElEOjNmYjMxYTQwLTQzZWMtNDM5NS04MmEyLWExM2ZkNDBjZDhhYQ=="
      },
      "oauth": {
        "clientId": "fc3504ddfec246ea9f67cc3431e09b8e",
        "clientSecret": "94c7cb84-915b-42c4-a7ac-4d26e777ba9d"
      },
    }
  },
};
 
export default mcsConfig;